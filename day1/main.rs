use std::fs;

fn main() {
    let input = fs::read_to_string("day1\\input.txt").expect("Could not read file");

    let lines: Vec<&str> = input.lines().collect();

    println!("Part1:");
    'outer1: for i in lines.iter() {
        let l1 = i.parse::<i32>().unwrap();
        for j in lines.iter() {
            let l2 = j.parse::<i32>().unwrap();
            if l1 + l2 == 2020 {
                println!("{}+{} = 2020\nProduct {}", l1, l2, l1 * l2);
                break 'outer1;
            }
        }
    }
    println!("\nPart2:");
    'outer2: for i in lines.iter() {
        let l1 = i.parse::<i32>().unwrap();
        for j in lines.iter() {
            let l2 = j.parse::<i32>().unwrap();
            for k in lines.iter() {
                let l3 = k.parse::<i32>().unwrap();
                if l1 + l2 + l3 == 2020 {
                    println!("{}+{}+{} = 2020\nProduct {}", l1, l2, l3, l1 * l2 * l3);
                    break 'outer2;
                }
            }
        }
    }
}

input = open("day2\input.txt", "r").readlines()
# print(len(input))

valid = 0
for i in input:
    a = int(i.split("-")[0]) - 1
    b = int(i.split(" ")[0].split("-")[1]) - 1
    key = i.split(" ")[1][0]
    password = i.split(" ")[-1]

    if (password[a] == key or password[b] == key) and (password[a] != password[b]):
        valid = valid + 1


print(valid)

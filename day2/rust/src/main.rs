use std::fs;

fn main() {
    let input = fs::read_to_string("..\\input.txt").expect("Could not read file");

    let lines: Vec<&str> = input.lines().collect();

    println!("Part1:");
    let mut valid = 0;
    for line in lines.iter() {
        let l: Vec<&str> = line.split_whitespace().collect();

        let min: i32 = l[0].split("-").collect::<Vec<&str>>()[0]
            .parse::<i32>()
            .unwrap();
        let max: i32 = l[0].split("-").collect::<Vec<&str>>()[1]
            .parse::<i32>()
            .unwrap();

        let key: char = l[1].chars().nth(0).unwrap();
        let pass: &str = l[2];

        let mut count = 0;
        for c in pass.chars() {
            if c == key {
                count += 1;
            }
        }
        if count <= max && count >= min {
            valid += 1;
        }
    }
    println!("Valid Passwords: {}\n", valid);
    println!("\nPart2:");
}
